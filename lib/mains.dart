import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'package:testnotify/green_page.dart';
import 'package:testnotify/red_page.dart';

/// RECEIVE MESSAGE WHEN APP IS IN BACKGROUND SOLUTION FOR
/// MESSAGE
Future<void> backgroundHandler(RemoteMessage message) async {
  print(message.data.toString());
  print(message.notification!.title);
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  /// FIREBASE INSTANCE BEFORE USE FIREBASEMESSAGING (OPTIONAL)
  FirebaseMessaging messaging = FirebaseMessaging.instance;

  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true
  );
  

  /// BACKGROUND-HANDLER
  FirebaseMessaging.onBackgroundMessage(backgroundHandler);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  /// THIS WIDGET IS THE ROOT OF YOUR APPLICATION
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Test Notification',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'HOME PAGE'),
      /// ROUTE TO PAGE
      /// AFFTER CLICK ON
      /// MESSAGE NOTIFICATION
      routes: {
        "red": (_) {
          return const RedPage();
        },
        "green": (_) {
          return const GrennPage();
        }
      },
    );
  }
}

// This is one of the full pretty sdnlksdlfnsdfnskdnfskjdnksj
class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //DO HERE FIRST
  @override
  void initState() {
    super.initState();
    /// GIVE YOU THE MESSAGE WHICH USER TAPS
    /// AND IT OPEN THE APP FROM TERMINATE STATE
    FirebaseMessaging.instance.getInitialMessage().then((message) {
      if (message != null) {
        final routeFromMessage = message.data["route"];
        Navigator.of(context).pushNamed(routeFromMessage);
      }
    });

    /// THE MESSAGE NOT ALERT IT WORK IN FORCE-GROUND
    FirebaseMessaging.onMessage.listen((message) {
      if (message.notification != null) {
        var notititle = message.notification!.title;
        print(notititle);
        print(message.notification!.body);
      }
    });

    /// MESSAGE ALERT WHEN THE APP IS IN BACKGROUND
    /// AND USER TAPS ON THE NOTIFICATION ROUTE TO PAGE TARGET
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      final routeFromMessage = message.data["route"];
      Navigator.of(context).pushNamed(routeFromMessage);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'notititle',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
    );
  }
}
