import 'package:flutter/material.dart';

class GrennPage extends StatefulWidget {
  const GrennPage({Key? key}) : super(key: key);

  @override
  _GrennPageState createState() => _GrennPageState();
}

class _GrennPageState extends State<GrennPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Green Page"),
      ),
      body: const Center(
        child: Text("Green Page"),
      ),
    );
  }
}
